import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from './usuario';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private authenticationUser: boolean = false;

  constructor(private router: Router) { }

  login(usuario: Usuario) {
    if (usuario.userName === 'testeMichelin' && usuario.password === 'michelin123') {
      this.authenticationUser = true;
      this.router.navigate(['/']);
    } else {
      this.authenticationUser = false;
    }
  }

  userIsAuthenticated() {
    return this.authenticationUser;
  }
  
}
